#
# This file contains testing of the MultiscaleManager.
# All tests can be run from the command line by:
#
# > python -m unittest discover -v -p '*UnitTest.py'
#
# To run an individual test with graphical output from ipython:
#
# import MultiscaleUnitTest as cgtest
# cgtc = cgtest.MultiscaleTestCase()
# cgtc.test_Multiscale()
#
import unittest

import PyCA.Core as ca
import numpy as np

# Test Class
#
class MultiscaleTestCase(unittest.TestCase):

    def __init__(self, methodName='runTest'):
        super(MultiscaleTestCase, self).__init__(methodName)

        self.cudaEnabled = (ca.GetNumberOfCUDADevices() > 0)

        # for all these, use "center of image" origin
        # grid for 2d image
        self.grid2D = ca.GridInfo(ca.Vec3Di(100, 128, 1),
                                  ca.Vec3Df(1.5, 2.5, 1),
                                  ca.Vec3Df(-(100.-1)/2*1.5, -(128.-1)/2*2.5, 0))
        self.grid2Dhalf = ca.GridInfo(ca.Vec3Di(50, 64, 1),
                                      ca.Vec3Df(3.0, 5.0, 1),
                                      ca.Vec3Df(-(50.-1)/2*3.0, -(64.-1)/2*5.0, 0))

        # grid for 3d image
        self.grid3D = ca.GridInfo(ca.Vec3Di(20, 24, 28),
                                  ca.Vec3Df(1.5, 2.5, 3.5),
                                  ca.Vec3Df(-(20.-1)/2*1.5, -(24.-1)/2*2.5, -(28.-1)/2*3.5))

        self.grid3Dhalf = ca.GridInfo(ca.Vec3Di(10, 12, 14),
                                      ca.Vec3Df(3, 5, 7),
                                      ca.Vec3Df(-(10.-1)/2*3.0, -(12.-1)/2*5.0, -(14.-1)/2*7.0))

    def skipIfNoCUDA(self):
        if not self.cudaEnabled:
            self.skipTest('Cannot run test, no CUDA device found or CUDA ' +
                          'support not compiled')

    ################################################################
    #
    # Begin Tests
    #
    ################################################################
    def test_Multiscale(self):
        mType = ca.MEM_DEVICE
        Im2Dorig = ca.Image3D(self.grid2D, mType)
        ca.SetMem(Im2Dorig, 4.0)
        Im3Dorig = ca.Image3D(self.grid3D, mType)
        ca.SetMem(Im3Dorig, 8.0)
        Im2D = ca.Image3D(self.grid2D, mType)
        Im3D = ca.Image3D(self.grid3D, mType)

        manager2D = ca.MultiscaleManager(self.grid2D)
        manager3D = ca.MultiscaleManager(self.grid3D)
        resampler2D = ca.MultiscaleResamplerGaussGPU(self.grid2D)
        resampler3D = ca.MultiscaleResamplerGaussGPU(self.grid3D)

        for scale in [2, 1]:
            manager2D.addScaleLevel(scale)
            manager3D.addScaleLevel(scale)

        # go to first scale
        scale = 0
        manager3D.set(scale)
        resampler3D.setScaleLevel(manager3D)
        manager2D.set(scale)
        resampler2D.setScaleLevel(manager2D)

        self.assertEqual(manager3D.getCurGrid(), self.grid3Dhalf)
        self.assertEqual(manager2D.getCurGrid(), self.grid2Dhalf)
        self.assertEqual(manager3D.getOrgGrid(), self.grid3D)
        self.assertEqual(manager2D.getOrgGrid(), self.grid2D)
        self.assertEqual(manager3D.getScaleFactor(), 2)
        self.assertEqual(manager2D.getScaleFactor(), 2)
        self.assertEqual(manager3D.getCurFactor(), ca.Vec3Df(2.0, 2.0, 2.0))
        self.assertEqual(manager2D.getCurFactor(), ca.Vec3Df(2.0, 2.0, 1.0))

        # downsample
        Im2D.setGrid(manager2D.getCurGrid())
        Im3D.setGrid(manager3D.getCurGrid())
        resampler2D.downsampleImage(Im2D, Im2Dorig)
        resampler3D.downsampleImage(Im3D, Im3Dorig)
        self.assertEqual(Im2D.grid(), self.grid2Dhalf)
        self.assertEqual(Im3D.grid(), self.grid3Dhalf)

        # print ca.MinMax(Im2Dorig)
        # print ca.MinMax(Im2D)
        # print ca.MinMax(Im3Dorig)
        # print ca.MinMax(Im3D)
        # print '2Dorig:', ca.Sum(Im2Dorig)/Im2Dorig.nVox()
        # print '3Dorig:', ca.Sum(Im3Dorig)/Im3Dorig.nVox()
        # print '2D:', ca.Sum(Im2D)/Im2D.nVox()
        # print '3D:', ca.Sum(Im3D)/Im3D.nVox()

        self.assertAlmostEqual(ca.Min(Im2D), 4.0, 5)
        self.assertAlmostEqual(ca.Max(Im2D), 4.0, 5)
        self.assertAlmostEqual(ca.Min(Im3D), 8.0, 5)
        self.assertAlmostEqual(ca.Max(Im3D), 8.0, 5)  # FAILS!

        # downsample in other order
        manager2D.set(scale)
        resampler2D.setScaleLevel(manager2D)
        manager3D.set(scale)
        resampler3D.setScaleLevel(manager3D)

        self.assertEqual(manager3D.getCurGrid(), self.grid3Dhalf)
        self.assertEqual(manager2D.getCurGrid(), self.grid2Dhalf)
        self.assertEqual(manager3D.getOrgGrid(), self.grid3D)
        self.assertEqual(manager2D.getOrgGrid(), self.grid2D)
        self.assertEqual(manager3D.getScaleFactor(), 2)
        self.assertEqual(manager2D.getScaleFactor(), 2)

        # downsample
        Im2D.setGrid(manager2D.getCurGrid())
        Im3D.setGrid(manager3D.getCurGrid())
        resampler2D.downsampleImage(Im2D, Im2Dorig)
        resampler3D.downsampleImage(Im3D, Im3Dorig)
        self.assertEqual(Im2D.grid(), self.grid2Dhalf)
        self.assertEqual(Im3D.grid(), self.grid3Dhalf)

        # print ca.MinMax(Im2Dorig)
        # print ca.MinMax(Im2D)
        # print ca.MinMax(Im3Dorig)
        # print ca.MinMax(Im3D)
        # print '2Dorig:', ca.Sum(Im2Dorig)/Im2Dorig.nVox()
        # print '3Dorig:', ca.Sum(Im3Dorig)/Im3Dorig.nVox()
        # print '2D:', ca.Sum(Im2D)/Im2D.nVox()
        # print '3D:', ca.Sum(Im3D)/Im3D.nVox()

        self.assertAlmostEqual(ca.Min(Im2D), 4.0, 5)  # FAILS!
        self.assertAlmostEqual(ca.Max(Im2D), 4.0, 5)  # FAILS!
        self.assertAlmostEqual(ca.Min(Im3D), 8.0, 5)
        self.assertAlmostEqual(ca.Max(Im3D), 8.0, 5)

        # Upsample
        scale = 1
        manager2D.set(scale)
        resampler2D.setScaleLevel(manager2D)
        manager3D.set(scale)
        resampler3D.setScaleLevel(manager3D)

        ca.SetMem(Im2D, 3.0)    # set ims to other values
        ca.SetMem(Im3D, 6.0)

        resampler2D.upsampleImage(Im2D)
        resampler3D.upsampleImage(Im3D)

        self.assertEqual(manager3D.getCurGrid(), self.grid3D)
        self.assertEqual(manager2D.getCurGrid(), self.grid2D)
        self.assertEqual(manager3D.getScaleFactor(), 1)
        self.assertEqual(manager2D.getScaleFactor(), 1)
        self.assertEqual(Im2D.grid(), self.grid2D)
        self.assertEqual(Im3D.grid(), self.grid3D)

        self.assertAlmostEqual(ca.Min(Im2D), 3.0, 5)
        self.assertAlmostEqual(ca.Max(Im2D), 3.0, 5)
        self.assertAlmostEqual(ca.Min(Im3D), 6.0, 5)
        self.assertAlmostEqual(ca.Max(Im3D), 6.0, 5)

        self.assertEqual(manager3D.getCurFactor(), ca.Vec3Df(1.0, 1.0, 1.0))
        self.assertEqual(manager2D.getCurFactor(), ca.Vec3Df(1.0, 1.0, 1.0))


    # runTest is only added so that the class can be instantiated
    # directly in order to call individual tests
    def runTest():
        print 'No tests to run directly, all are member functions'
