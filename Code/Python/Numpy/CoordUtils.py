
import numpy as np
import itertools


def getcoorditer(start_or_end, end=None):
    """return iterator over multi-dimensional coordinates

    getcoorditer((5,3)) returns an iterator over all coordinates of a
    5-by-3 array

    getcoorditer((1,1), (5,3)) returns all coordinates of the subarray
    somearray[1:5,1:3], ie [(1,1), (1,2), (2,1), (2,2), ... (4,2)]

    """

    ndims = len(start_or_end)

    if end is None:
        start = [0]*ndims
        end = start_or_end
    else:
        assert len(end) == ndims
        start = start_or_end

    iterlist = [xrange(s, e) for s, e in zip(start, end)]
    return itertools.product(*iterlist)


def gridcoords(sz):
    """want m-by-n-by-2 or m-by-n-by-d-by-3 array of coordinates

    """
    coordlist = [np.arange(dsz) for dsz in sz]
    return meshcoords(*coordlist)


def meshcoords(*args):
    """takes a number of arrays (say N) each having size S_n, and returns
    grid coordinates of size S_0-by-S_1-by-...-by-S_(N-1)-by-N

    eg, meshcoords([0,1,2,3], [0,1,2]) returns a 4-by-3-by-2 array,
    while meshcoords([0,1,2,3], [0,1,2], [0,1,2,3,4]) returns a
    4-by-3-by-5-by-3 array

    """
    coords = np.meshgrid(*args, indexing='ij')
    g = np.stack(coords, axis=-1)
    return g


def add_axis(arr, dim):
    """Add and axis to arr at the given dim

    e.g. if arr.shape is (5,2) then:
    add_axis(arr, 0).shape is (1,5,2)
    add_axis(arr, 1).shape is (5,1,2)
    add_axis(arr, 2).shape is (5,2,1)
    add_axis(arr, -1).shape is (5,2,1)
    """
    ndims = len(arr.shape)
    slicelist = [slice(None) for _ in range(ndims+1)]
    slicelist[dim] = np.newaxis
    return arr[tuple(slicelist)]
